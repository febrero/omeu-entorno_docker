# Utiliza la imagen base de Alpine Linux
FROM alpine:edge

# Instala las herramientas y dependencias necesarias
RUN apk add --no-cache \
  bash \
  git \
  lua \
  nodejs \
  npm \
  lazygit \
  bottom \
  python3 \
  go \
  neovim \
  ripgrep \
  alpine-sdk \
  zsh

# Instala las fuentes Nerd Fonts
#RUN git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git /tmp/nerd-fonts && \
#  cd /tmp/nerd-fonts && \
#  ./install.sh

# Limpia el directorio temporal
#RUN rm -rf /tmp/nerd-fonts


# Instala Oh My Zsh
RUN sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# instalando a miñas cositas
#RUN sh -c "$(curl -fsSL https://gitlab.com/FFebrero/instalar/-/blob/d768c13ee1ce07cd3ff5204646532c42d30c91f3/instalar.sh)"


# Establece el directorio de trabajo
WORKDIR /root

# Clona el repositorio principal
RUN git clone --depth 1 https://github.com/AstroNvim/AstroNvim ~/.config/nvim

COPY astrovimconfig /root/.config/nvim/lua/user

# Instala fuentes necesarias
RUN apk add --no-cache fontconfig font-noto-emoji

# Instala Powerlevel10k
RUN git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.oh-my-zsh/custom/themes/powerlevel10k

# Clona el repositorio de configuración de Powerlevel10k
RUN git clone --depth 1 https://gitlab.com/febrero/p10k_config.git ~/.p10k_config


# Configura Zsh para usar Powerlevel10k como tema
RUN sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="powerlevel10k\/powerlevel10k"/' ~/.zshrc
# Configura Zsh para usar Powerlevel10k y tu configuración personalizada
RUN sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="powerlevel10k\/powerlevel10k"/' ~/.zshrc && \
  echo "source /root/.p10k.zsh" >> ~/.zshrc

# Copia la configuración de Powerlevel10k al directorio adecuado
RUN cp -r ~/.p10k_config/.p10k.zsh /root


# Copia el script que solicitará usuario y contraseña
COPY clone_script.sh /root/clone_script.sh

# Otorga permisos de ejecución al script
RUN chmod +x /root/clone_script.sh

# Configura Zsh como shell predeterminado
#RUN chsh -s /bin/zsh
# Cambia la shell predeterminada a Zsh en /etc/passwd
RUN sed -i '/root/s/\/bin\/ash/\/bin\/zsh/' /etc/passwd

# Ejecuta el script
CMD ["/bin/zsh", "-1", "/root/clone_script.sh"]

